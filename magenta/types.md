# types
we have all the classics: `bool`, `string`, (`u`)`int`(`8`|`16`|`32`|`64`), `float`, and `double`
> `int` and `uint` imply `int16` and `uint16` by default

> you can also use `byte` and `sbyte` for `uint8` and `int8` respectively

to represent an array, simply append a couple of square brackets to the end of an existing type (ie. `type[]`)
> you can create multi-dimensional arrays by adding multiple couples of brackets, ie. `type[][]` is 2d, `type[][][]` is 3d, etc.
if an array has a specified length, it will be initialized as a full array of default values. only the first series of dimensions in a multi-dimensional array may be given lengths

magenta does not have null (explained below) but does have a `void` type
> `void` is the return type of a function that returns nothing, it cannot be used as a type for a variable/field

## booleans
the value of a `bool` can only be `true` or `false`

the bool has magenta's only unary operator, the `!` (not)
```cs
!false or false!   → true
!true  or true!    → false

let myBool bool; !myBool or myBool!
  → true

!isArray([]) or isArray([])!
  → false
```
> there is no "internal truthiness" that may be revealed with `!`. it is only allowed next to booleans, and will error if placed next to any other type

# default values
magenta lacks the concept of any `nil` or `undefined` values. instead, when a variable or field is defined without an explicit value, it takes on its "default value". a few examples;
- `var x string`: `x` has the value of empty string (`""`)

- `var y int`: `y` has the value of zero (`0`)

- `var z Guy`: a struct is initialized into `z` with default values for all of its members (`new Guy()`). **this will error if the struct has any `required` fields**
