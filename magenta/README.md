# magenta language specification
previously all aspects of this language were in one massive file and it got a bit confusing since it was trying to be a full specification covering everything but also short and sweet to the point it wasn't either. if you just want a quick overview of the core syntax of the language look no further then the *lilascript* section of this documentation. if you are looking to familiarize yourself intimately with magenta to the point you would be able to implement it then look no further :>

> as of writing this is entirely as incomplete and confusing as it was before. please bare with me as i write the lilascript docs first and hone them

* [enums](enums.md)
* [errors](errors.md)
* [flow](flow.md)
* [functions](functions.md)
* [loops](loops.md)
* [math](math.md)
* [memory](memory.md)
* [namespacing](namespacing.md)
* [structs](structs.md)
* [types](types.md)
* [variables](variables.md)
