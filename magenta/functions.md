# functions
a function is defined with its returned type followed by the name and list of arguments enclosed in parenthesis. arguments are defined name first followed by their type. if the function can return an error, it can be signified with `throws`. it is then followed by a block
```cs
int divide(x int, y int) throws MathError {
	y == 0 && throw "divide by zero"
	return x / y
}
```

# anonymous functions
an anonymous function is defined through an f, traditional list of argument, error type, and block
```cs

```

# function types
there is an `func` type to represent functions. by default it represents a function that returns `void` and takes no args. you can specify a return type by appending `<type>`, specify a list of argument types by appending `[type, type, ...]`, and specify a `throws` with `!error`. for example;
```cs
typeof(f() {})
  → func
  → func<void>

typeof(f() { return 1 })
  → func<int>

typeof(divide)
  → func<int>[int, int]!MathError
```
