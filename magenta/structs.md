# structs
structs are defined in struct blocks. to make one first you open it with `struct StructName {`, then you can enter in a comma deliminated list of your struct's fields, and close it with `}`

a field has the following syntax (square brackets implying optional)
```
FieldName [const] [required] T [= defaultValue],
```
- `T` is optional when there is a default value
- if `const` is present, that field's value can only be set at initialization time, ie. `new Struct([field value])`
- if `required` is present, that field's value must be set at initialization time, ie. `new Struct(field value)`

```cs
// example of a struct definition
struct Guy {
	name = "guy",
	age int,
	gay const = true,
	sayName func,
	awesome required const, // trailing comma allowed on the last field!!
}
```

to initialize a struct, call it as a function with the new keyword, and assign it to a variable, ie. `new Struct(field0 value0, field1 value1, etc...) -> var myStruct`

if a function is a type of a field on a struct — that is a method, which must be defined. to define a method for a struct, simply define the method as a function called the field of the struct, ie. `T Struct.myMethod() { ... }`

you can define a constructor by writing a function with the return type and name of the corrosponding struct, ie. `Guy Guy() { ... }`
