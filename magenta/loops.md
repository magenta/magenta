# loops
magenta two kinds of loops, the `for` and the `do` loop. the `for` loop behaves as a standard c-style `for` loop, but also doubles as a traditional `while` loop if you exclude the brackets
```cs
for (0 -> var i; i < 5; 1 +-> i) {
	tty.writeln(i | math.itoa)
}  → 0, 1, 2, 3, 4

0 -> var x
for x < 5 {
	tty.writeln(1 +-> i | math.itoa)
}  → 1, 2, 3, 4, 5
```

the `do` loop acts as a traditional c-style `do while` loop, executing the contents of the block at least once, but the statement stays in the same position
```cs
0 -> var x // x is a global variable

void loop() {
	do x < 5 {
		tty.writeln(1 +-> i | math.itoa)
	}
}

loop()
  → 1, 2, 3, 4, 5

loop()
  → 6 // if a for loop had been used, there would have been no output this second time
```
