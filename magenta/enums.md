# enumerators
in magenta, an enum is an array of unique strings that when used as a type, restricts what contents a string might have, like in typescript

the format for an enum consists of an opening definition eg. `enum MyEnum {`, a set of strings eg. `"yes" "maybe" "possibly" "no"`, and a closing bracket followed by the index of the default value (not required, default is `0`)

eg. now you can type a function as returning `MyEnum`, which you can expect to return one of those strings
```cs
enum MyEnum {
	"yes" "maybe" "possibly" "no" }

var x MyEnum
  → "yes" // x is initialized, by default using the first enum value

"maybe" -> x
// (value of x is updated)

"definitely" -> x
  → compiler error: "definitely" is not a value of MyEnum
```
