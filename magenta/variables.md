# variables
variables can be defined by using a var statement in the format `var variableName T` (eg. `var name string`)
> including the type in the statement is optional if you assign to the variable right away, as the type can be inferred

a constant can be defined by using a let statement in the format `let constantName T` (eg. `let age byte`), once a value is assigned to a constant it cannot be mutated
> if you do not assign to the constant right away, it will assume its type's default value, and will let you set it later

## assignment
to assign to a variable, constant, or field you can use the arrow syntax in the format `value -> variable` (eg. `"hunter" -> var name` or `"joe" -> instaceOfAPersonStruct.firstName`)
> as magenta always executes left to right, you may not use `<-` to make "reverse assignments"
