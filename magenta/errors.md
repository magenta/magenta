# error handling
any enum can be used as an error type, to be able to return an error, add a `throws` keyword onto your function definition followed by an enum, then you can now `throw` a string from that enum, for example;
```cs
enum NavigationError {"ok" "notExist" "toFile"}

string cd(path string) throws NavigationError {
	stat(path) -> var pathStat
	|> "notExist" : throw "notExist"
	|> "file"     : throw "toFile"

	... // return the new full current path
}
```

to handle thrown errors, you can use a catch statement, which is identical to a switch statement, except using `!>` instead of `|>`. to use a switch statement after a catch statement, you can denote that you have stopped catching errors with `!> default |>`
```cs
cd("thisPathDoesNotExist")
!> "notExist" : tty.writeln("invalid path")return
!> "toFile" {
	getFileParentFolder()
	!> ... : ... // conditionals and/or further errors possible in blocks
	return
}> default |> // it is cleaner to put the |> here
   ... : ...
|> ... { ...
}> ... { ...
}
```

if you just want something to catch errors but no error in particular, you can use the single line catch `!!`. a break/return statement is required at the end of the handling expression(s), as well as a semicolon
```cs
math.sqrt(-1) !! tty.println; -> var squareroot
  → prints "sqrt of negative number"
  → squareroot is not assigned to
```
