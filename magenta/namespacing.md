# namespacing
at the top of each magenta source file, magenta expects a namespace. at compile time the working directory will be scanned for magenta files and the one in the namespace `main` will be chosen as the entry point. there may be multiple files belonging to a certain namespace, but only one file may be in the `main` namespace!
```cs
namespace main
```

then after the `namespace` decleration, you may include libraries
```cs
// include libraries and namespaces
include tty
include err
include math
include loop
include arrays
include myUtility // eg. some other namespace in the project folder

// can be written in one neat line
include tty err math loop arrays myUtility
```

you may alias an included namespace, but then you cannot include any other namespaces in that line
```cs
include myUtility.errors as myErrors

// these are equivalent
myUtility.errors.GayError
myErrors.GayError
```

it can also be useful when writing a library
```cs
namespace myLib
include myLib.errors as errors
```
