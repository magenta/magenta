# memory management
magenta is immutable where convenient, and has no references. there is no manual memory management, and it implements escape analysis as its sole garbage collection mechanism

```cs
// initializes memory for an unitialized struct
var globalHomer Guy

string getName() {
	// initializes a new Guy with name "lisa" into new variable 'guy', assigning new memory
	new Guy(name "lisa") -> var guy

	// creates a copy of guy into a new variable 'otherGuy', assigning new memory
	guy -> var otherGuy

	"homer" -> guy.name // updates the name of the copy

	// copies 'guy' over 'globalHomer', changing memory
	guy -> globalHomer

	"asdf" -> guy.name // update the name of the copy again

	// 'otherGuy' does not reflect name changes of 'guy'
	return otherGuy.name // "lisa"

	// upon end of the function, all allocated memory is freed (in this case 'guy' and 'otherGuy')
}

// globalHomer has its default values
globalHomer.name | tty.println

// getName() is ran, returning the name of 'otherGuy'
// it also assigns 'guy' to 'globalHomer'
getName() | tty.println // "lisa"

// only the first update to 'guy' is reflected in 'globalHomer'
globalHomer.name | tty.println // "homer"
```
