# arithmetic
magenta has the following math operators, which can only be used for numerical values of the *same type*
> when dividing integers, anything after the decimal point will be cut off
```cs
x + y     x * y
x - y     x ^ y // power (xʸ)
x / y     x % y // remainder (of x⁄y)
```

there is a shorthand for assignment with a single operation, `y $ x -> y`, with `x $-> y` where `$` is an operator above, eg. `x ^-> y` == `y ^ x -> y`

arithmetic in magenta is done left to right, there is no operator precedence
```cs
// bimdas
100  +  30  * 3   → 190
100  + (30  * 3)  → 190
(100 +  30) * 3   → 390

// magenta
100  +  30  * 3   → 390
100  + (30  * 3)  → 190
(100 +  30) * 3   → 390
```
> *bimdas* order - ( ), ^, %, *, /, +, -

> in magenta, brackets do not get calculated first
