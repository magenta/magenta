# conditionals
magenta has the following conditionals
```cs
any  == any      int  >= int
any  != any      int  <= int
int  >  int      bool && bool
int  <  int      bool || bool
```

as logical expressions are evaluated left to right, they are tested for possible "short-circuit" evaluation
> `a() && b()` would not call `b` if `a` returned false

> `a() || b()` would not call `b` if `a` returned true

magenta has if statements and ternaries that work just like how they would in any other c-style language

# piping
the pipe character `|` pipes the expression on the left to the one on the right, ie. `from | to`, usually `to` is a `func` but other types allow piping into them as well
```cs
32 + 32 | math.sqrt              → 8
32 + 32 | math.sqrt | math.itoa  → "8"
```

you can only pipe one argument at once, if you need to pipe to a function that takes multiple required arguments, you can use a dollar sign as one of the arguments to represent the piped value
```
4 | math.pow()
```

# if statements
magenta has your regular if/else statements
```cs
if (condition) {
} else (condition) {
} else {
}
```

thanks to short circuit evaluation, you can utilize `&&` and `||` as shorthand for `if condition` and `if !condition` respectively

# switch statements
then we have switch statements. switch statements comprimise a set of cases that follow a function call, a case has the syntax
```
start condition separator [end]
```
> `start` is either `|>` for a single line case, or `}>` to end a multi line case

> `condition` is an expression that has the output of the function piped into it

> `separator` is either `:` to start a single line case, or `{` to start a a multi line case

> you may `end` the final case with a single `}` if it is a multi line case

for example;
```cs
getInput("password")
|> "good"  {
	login()
	return 0
}> "short" : tty.writeln("too short")
|> "long"  : tty.writeln("too long")
|> else    {
	tty.writeln("password is otherwise invalid")
	// this last case could also be written as:
	// |> else : tty.writeln("...")
}
```
