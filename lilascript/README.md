# lilascript
lilascript is a subset of magenta for prototyping and scripting

lila (pronounced lih-lah) means purple in hungarian

[→ implementation](https://codeberg.org/magenta/lilascript)

## subset design
* no namespacing
* no error handling
* 1D arrays only
* no anonymous functions
* can't pass functions as arguments
* no `double`
* no `64`-bit integers
* simpler typing system
* simpler preprocessor

# specifcation
## comments
denoted with `//`, or `/*` and `*/`

## variables
`var x T` — create variable `x` of type `T`

`y -> var x` — create variable `x` of `y`'s type, assign `y`

## math
`+`, `-`, `*`, `/`, `^` (to power), `%` (remainder of division)

math operators can only be used on two numeric values of the same type, and do not have precidence over one-another

math operators can be used with an assignment as a shorthand, like `1 +-> y`

## types
## enumerators
`enum myEnum { "zero" "one" "two" "three" } 1` — define `myEnum` with four values, default being `myEnum.one`

an enum must have at least two values, and a number other then zero at the end of the block sets a default value

a variable of an enum type may be assigned a string of the name of a value in the enum, or compared to a string with the value of an enum, and it is treated as if compared to the full identified of the enumerator. you may *not* compare an enum value to a string *variable*, nor perform any string-like operations with it (other then concatenation)

### void
lilascript forgoes null, but to define a function that doesn't return a value, you may use `void` to define it

### strings
strings are specified with `"double quotes"`, and have a default value of an empty string

`+` may be used to concatenate strings

### boolean
the `bool` can only be `true` or `false`, and default to `false`

only function of `!` is used to invert a boolean

### numbers
all numbers default to zero

`float` — a number with decimal precision

(`u`)`int`(`8`|`16`|`32`) — an (*unsigned*) integer (with *n* bits)
> `int` is shorthand for int16, `uint` is shorthand for uint32

`byte`/`sbyte` — shorthand for `uint8`/`int8` respectively

## arrays
`T[]` — dynamic-length array of `T` values

`T[n]` — fixed-length array of `T` values, initialized to `n`x defaults. `n` must be known at compile time

### functions
c-style function declerations, argument names come before types

`func`, `func<void>` — type of function that returns nothing, takes no arguments

`func<int>[T, T1]` — type of function that returns an `int16`, and takes two arguments of differing types
