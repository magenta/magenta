# magenta
magenta is designed to make writing (graphical) applications fast, simple, and fun

this is the repository for the documentation of all language features as well as additional specifications for implementation, for more info see [the website](https://magenta.zvava.org) (‼️ not going to be up to date any time soon)

currently working on the prototype [lilascript](https://codeberg.org/magenta/lilascript)

## design
magenta's syntax is inspired by the bourne shell, f#, typescript, and go. it is has the following core concepts;
* strongly typed, weakly immutable
* no manual memory management, stupid simple garbage collector
* be concise *and* legible to the scanning eye
* no references
* no null
* do exactly what you tell it to, no funny business
* most importantly, be fun to write in
